var origBoard;
var win=0, lose=0, tie=0;
const huPlayer = 'O';
const aiPlayer = 'X';
const winCombos=[
    [0, 1, 2],      
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],      // TRES EN RAYA VERTICAL
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],      //TERS EN RAYA DIAGONAL
    [6, 4, 2],      //TRES EN RAYA DIAGONAL
];

const cells=document.querySelectorAll('.cell');
startGame();

function startGame(){
    document.querySelector(".endgame").style.display="none";
    origBoard=Array.from(Array(9).keys());
    for(var i=0;i<cells.length;i++){
        cells[i].innerText='';
        cells[i].style.removeProperty('background-color');
        cells[i].addEventListener('click',turnClick, false);
    }
    
}
function turnClick(square){
    if(typeof origBoard[square.target.id] =='number'){
        turn(square.target.id, huPlayer);
        if(!checkTie()) turn(bestSpot(),aiPlayer);
    }
    
}

function turn(squareId, player){
    origBoard[squareId]= player;
    document.getElementById(squareId).innerText=player;
    let gameWon=checkWin(origBoard, player)
    if(gameWon) gameOver(gameWon)
}
function checkWin(board, player){
    let plays=board.reduce((a, e, i) => (e===player)?a.concat(i) : a, []);
    let gameWon = null;
    for(let[index,win] of winCombos.entries()){
        if(win.every(elem => plays.indexOf(elem) > -1)){
            gameWon={index: index,player: player};
            
            break;
        }
    }
    return gameWon;
}
function gameOver(gameWon){
    for(let index of winCombos[gameWon.index]){
        document.getElementById(index).style.backgroundColor = gameWon.player ==huPlayer?"blue":" orange";
 }
 //Evitar  escritura ,ye existe ganador
    for(var i=0;i<cells.length;i++){
        cells[i].removeEventListener('click',turnClick,false);
    }
    declareWinner(gameWon.player == huPlayer ? "Ganaste!!!"+ win++ : " Perdiste, PC gana"+ lose++ );
   document.getElementById("scoreWin").innerHTML=win;
   document.getElementById("scoreLose").innerHTML=lose;
}

function declareWinner(who){
document.querySelector(".endgame").style.display = "block";
document.querySelector(".endgame .text").innerText = who;
}
function emptySquares(){
    return origBoard.filter(s => typeof s =='number');
}
function bestSpot(){
    return emptySquares()[0];
}

function checkTie(){
    if(emptySquares().length == 0){
        for(var i=0; i<cells.length;i++){
            cells[i].style.backgroundColor="green";
            cells[i].removeEventListener('click',turnClick,false);
        }
        tie++;
        declareWinner("EMPATE"+ tie);
        document.getElementById("scoreTie").innerHTML=tie;
        return true;
    }
    return false;
}

    
 function getScore(score){
     if(declareWinner()=="EMPATE"){
         tie++;
          document.getElementById("scoreTie").innerHTML=tie;
     }else{
         if (declareWinner()=="Ganaste") {
            win++; 
            document.getElementById("scoreWin").innerHTML=win;
         }
         else{
             document.getElementById("scoreLose").innerHTML=lose;
         }
     }
 }
 function saveScore(){
     
    var textToWrite1= " Jugador gana: "+ document.getElementById("scoreWin").innerHTML;
      textToWrite1+= "empata "+document.getElementById("scoreTie").innerHTML;
      textToWrite1+=" pierde "+ document.getElementById("scoreLose").innerHTML;

    //especifica el nombre del archivo a  guardar
    var contentBlob= new Blob([textToWrite1],{type:'text/plain'});
    //guardar  txt cion nombre
    var fileName="score.txt";
    //link click
    var downloadLink=document.createElement("a");
    //nombre link
 downloadLink.download=fileName;
 //texto link oculto
    downloadLink.innerHTML="Hiden link";
    window.URL=window.URL||window.webkitURL;
    //crear link objecto
    downloadLink.href=window.URL.createObjectURL(contentBlob);
    //DOM llama a sec. archivo elimina funccion
    downloadLink.onclick=destroyClickedElement;
    //hipervinculo oculto
    downloadLink.style.display="none";
    document.body.appendChild(downloadLink);

    //clic nuevo link
    downloadLink.click();
}
function destroyClickedElement(event)
{
// remover el link del DOM
    document.body.removeChild(event.target);
}
